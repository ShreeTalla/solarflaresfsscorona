import os
import random
from data_cleaning import clean_data


class Undersampling:
    """
    Undersampling data using climatelogy-preserving method.
    """
    def __init__(self, file_path: str, types: list, features: list):
        """
        :param file_path: partition data
        :param types: flare and non-flare
        :param features: list of features
        """
        self.file_path = file_path
        self.types = types
        self.m = list()
        self.x = list()
        self.fq = list()
        self.b = list()
        self.c = list()
        self.a = list()
        self.final_list = list()
        self.features = features

    def get_flare_files(self, type):
        """
        get the list of mts csv files for each type.
        :param type: flare or non-flare
        :return: list of mts files and path
        """
        fl = self.file_path+"//"+type
        fl_list = os.listdir(fl)
        return fl_list, fl

    def set_sub_flares_to_list(self):
        """
        based on the file name partition data is split into 5 sub classes
        M, X, B, C, FQ.
        :return: none
        """
        for type in self.types:
            fl_list, fl_path = self.get_flare_files(type)
            for mts in fl_list:
                if mts[:1] == 'M':
                    self.m.append(fl_path+"//"+mts)
                if mts[:1] == 'X':
                    self.x.append(fl_path+"//"+mts)
                if mts[:1] == 'B':
                    self.b.append(fl_path+"//"+mts)
                if mts[:1] == 'C':
                    self.c.append(fl_path+"//"+mts)
                if mts[:2] == 'FQ':
                    self.fq.append(fl_path+"//"+mts)

    def undersample_subflares(self):
        """
        Appling climatelogy data is sampled for each subflare.
        :return: none
        """
        m = len(self.m)
        x = len(self.x)
        b = len(self.b)
        c = len(self.c)
        fq = len(self.fq)
        m_ratio = int((m / (m + x))*(m + x))
        x_ratio = int((x / (m + x))*(m + x))
        b_ratio = int((b / (b + c + fq))*(m + x))
        c_ratio = int((c / (b + c + fq))*(m + x))
        fq_ratio = int((fq / (b + c + fq))*(m + x))
        difference = (m_ratio+x_ratio)-(b_ratio+c_ratio+fq_ratio)
        if difference > 0:
            fq_ratio += difference
        random.shuffle(self.m)
        self.sampling_sub_flares(m_ratio, self.m, 0)
        print("sampling M done")
        random.shuffle(self.x)
        self.sampling_sub_flares(x_ratio, self.x, 0)
        print("sampling X done")
        random.shuffle(self.b)
        self.sampling_sub_flares(b_ratio, self.b, 1)
        print("sampling B done")
        random.shuffle(self.c)
        self.sampling_sub_flares(c_ratio, self.c, 1)
        print("sampling C done")
        random.shuffle(self.fq)
        self.sampling_sub_flares(fq_ratio, self.fq, 1)
        print("sampling FQ done")
        return m_ratio, x_ratio, b_ratio, c_ratio, fq_ratio

    def sampling_sub_flares(self, req_len, list, type):
        for i in (list[:req_len]):
            matrix = clean_data.csv_matrix(i,type, self.features)
            self.final_list.append(matrix)
