import numpy as np
import pandas as pd
import os

class MTSDataset:
    """
    This class gets all the .csv files and creates a list of json
    {
        MTS_item: dataframe,
        file: file_name,
        Class : M/A/B/C/X/FQ,
    }
    """
    def __init__(self, path, n):
        """
        :param path: path to partitions folder
        :param n: number of required features
        """
        self.path = path
        self.n = n+1
        self.list = []

    def csv_2_correlation_matrix(self, csv_path):
        """
        This method converts the raw csv to correlation matrix for each MTS item.
        :param csv_path: Path to csv file
        :return: correlation co-efficient matrix.
        """
        mts_item = pd.read_csv(csv_path, delimiter='\t')
        mts_df = pd.DataFrame(mts_item.iloc[:, 1:self.n])
        mts_df = mts_df.apply(pd.to_numeric, errors='coerce')
        mts_clean = mts_df.fillna(0)
        return np.asmatrix(mts_clean.corr())

    def csv_2_jsonList(self, types):
        """
        This method converts the all the csv files in partition data to list of json.
        :param type: either folder FL or NF or list of folders
        :return: None
        """
        for type in types:
            fl = self.path+"//"+type
            fl_list = os.listdir(fl)
            for mts in fl_list:
                self.list.append({
                    "MTS_item": self.csv_2_correlation_matrix(fl+"//"+mts),
                    "file": mts,
                    "Class": mts[:2] if mts.startswith('FQ') else mts[0]
                })


# def main():
#     c2l = MTSDataset(r'D:\GSU_Assignments\Summer_sem\6999\mvts_fss_st\data\partition1_instances\partition1', 24)
#     c2l.csv_2_jsonList(["NF"])

# if __name__ == "__main__":
#     main()
