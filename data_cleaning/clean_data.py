import numpy as np
import pandas as pd
import os


def csv_matrix(path: str, flare_type: str, features: list) -> list:
    """
    Converts csv file to matrix and cleans if any NAN values are present in dataframe
    using backward fill and forward fill.
    :param path: path of MVTS excel file.
    :param flare_type: flare(0) or non-flare(1).
    :param features: list of features.
    :return: list of mts item 2d-array and flare type.
    """
    mts_item = pd.read_csv(path, delimiter='\t')
    mts_df = pd.DataFrame(mts_item[features])
    mts_clean = mts_df.bfill().ffill()
    mts_clean = mts_clean.to_numpy()
    return [np.nan_to_num(mts_clean), flare_type]


def folder_2_dataset(path: str, flare_types: list, features: list) -> list:
    """
    Get all the data from partition file and forms a 2d list of size
    number of flares*2.
    :param path: partition folder
    :param flare_types: flare(0) and non-flare(1)
    :param features: list of features.
    :return: list of mvts items and their respective types.
    """
    flare_data = list()
    for ft in flare_types:
        fl = path + "//" + ft
        fl_list = os.listdir(fl)
        if ft == "NF":
            ft = 1
        else:
            ft = 0
        for mts in fl_list:
            flare_data.append(csv_matrix(fl + "//" + mts, ft, features))
    return flare_data
