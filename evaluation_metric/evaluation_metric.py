def hss(cm):
    """
    :return: Heidke skill score. Following sklearn's implementation of other metrics, when
    the denominator is zero, it returns zero.
    """
    tn, fp, fn, tp = cm[0], cm[1], cm[2], cm[3]
    numer = 2 * ((tp * tn) - (fn * fp))
    denom = ((tp + fn) * (fn + tn)) + ((tp + fp) * (fp + tn))
    hss = (numer / float(denom)) if denom != 0 else 0
    return hss


def tss(cm):
    """
    :return: true skill statistic. Following sklearn's implementation, when
    the denominator is zero, it returns zero.
    """
    tn, fp, fn, tp = cm[0], cm[1], cm[2], cm[3]
    n = tn + fp
    p = tp + fn
    tp_rate = tp / p if p > 0 else 0
    fp_rate = fp / n if n > 0 else 0
    return tp_rate - fp_rate