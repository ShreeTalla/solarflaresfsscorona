import numpy as np

class Vectorize:
    """
    This class vectorize a correlation coefficient matrix using the upper triangle.
    """

    def __init__(self, C, n):
        """
        :param C: a correlation coefficient matrix of an n-variate MTS item.
        :param n: number of required features
        """
        self.C = C
        self.n = n

    def vectorize_matrix(self):
        """
        This method vectorizes the upper triangle of correlation coefficient matrix.
        :return: vectorized data
        """
        upper_index = np.triu_indices(self.n, 1)
        return self.C[upper_index]
