class SplitTrainTest:
    """
    This class splits the dataset into test, train and validation.
    """
    def __init__(self, dataset, percent, validation_percent = 0, validation = False):
        """
        :param dataset: json list of MTS item
        :param percent: training percentage [0-100]
        :param validation_percent: validation percent [0-100]
        :param validation: true if validation data is needed, vise versa
        """
        self.dataset = dataset
        self.percent = percent / 100
        self.vPercent = validation_percent / 100
        self.validation = validation

    def split(self):
        """
        splits the data into train and test by default, if validation is true then splits the data into 3 parts.
        :return: the train, test or train, validation and test data.
        """
        train_length = int(len(self.dataset)*self.percent)
        validation_length = int(len(self.dataset)*self.vPercent) if self.validation and self.vPercent !=0 else 0
        if validation_length != 0:
            return self.dataset[:train_length], self.dataset[train_length:validation_length], self.dataset[validation_length:]
        else:
            return self.dataset[:train_length], self.dataset[train_length:]

