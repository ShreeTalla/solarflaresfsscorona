import numpy as np
from sklearn.feature_selection import RFE
from sklearn.svm import SVC

from feature_selection.vectorize import Vectorize


class Corona:
    """
    This class is implementation of algorithm 3 from the research paper in paper/corona.pdf
    1. It creates the correlation coefficient matrix for MTS item.
    2. Vectorizes the above obtained matrix.
    3. Assign weights to matrix using SVM.
    4. Ranks the variables and eliminates based on recursion.
    """

    def __init__(self, MTS_dataset, n, k):
        """
        :param MTS_dataset: MTS dataset of SWAM_SF partion by parition
        :param k: the required number of variables.
        :param n: number of required features
        """
        self.MTS_dataset = MTS_dataset
        self.N = len(MTS_dataset)
        self.k = k
        self.n = n

    def feature_matrix(self):
        """
        This method creates a 2d array of size N mts items x length of mts vector.
        :return: feature matrix
        """
        feature_matrix = []
        for mts_json in self.MTS_dataset:
            vectorize = Vectorize(mts_json[0], self.n)
            vector = np.array(vectorize.vectorize_matrix()).flatten()
            feature_matrix.append(vector)
        self.features = np.nan_to_num(feature_matrix)

    def get_classes(self):
        """
        :return:  the classes of MTS dataset.
        """
        classes = []
        for mts_json in self.MTS_dataset:
            classes.append(mts_json[0])
        self.classes = classes


    def rank_variables(self):
        """
        This method ranks each correlation matrix pair weight a rank.
        :return: ranks of vector
        """
        estimator = SVC(kernel='linear')
        rfe = RFE(estimator, self.k, step=1)
        ranks = rfe.fit(self.features, self.classes)
        return ranks.ranking_

    def de_vectorize_ranks(self, ranks):
        """
        vector ranks are converted into symmetric matrix and obtain the ranks of each feature by column-wise summation.
        :return: ranks of features.
        """
        symmetric_matrix = np.zeros((24, 24))
        symmetric_matrix[np.triu_indices(24, 1)] = ranks
        symmetric_matrix[np.tril_indices(24, -1)] = ranks
        row, col = np.diag_indices(24)
        symmetric_matrix[row, col] = np.array([1.00])
        return np.sum(symmetric_matrix, axis=0)




